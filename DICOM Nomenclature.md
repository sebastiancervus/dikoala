This file serves to define the nomenclature related to DICOM header elements.
The intended goal is to use nomenclature that is aligned with the DICOM
standard. The following documents serve as the reference:
[https://dicom.nema.org/medical/dicom/current/output/html/part02.html](https://dicom.nema.org/medical/dicom/current/output/html/part02.html)
[https://dicom.nema.org/medical/dicom/current/output/html/part05.html#chapter_3](https://dicom.nema.org/medical/dicom/current/output/html/part05.html#chapter_3)

Note that this document is far from being a comprehensive summary of the DICOM standard. It only outlines the aspects that are relevant for DiKoala.

DiKoala looks at **Data Elements** in the DICOM header. A **Data Element** is composed of the following constituents:

- A **Data Element Tag** that serves as a unique identifier. The **Data Element Tag** consists of a tuple of two numbers, (*x*, *y*), where *x* is called **Group Number** and *y* is called **Element Number**. Typically, both *x* and *y* are given in hexadecimal representation, but wihout the `0x` prefix. 
- A **Value Field**, which holds the value of the Data Element. This can be a date or time, a piece of patient information, or an acquisition parameter. The data can be stored in one of several available formats (see **Value Representation** below).
- The  **Value Representation** indicates the format in which a piece of information is stored in a **Value Field**. It is represented as a two-character string. For example, "DT" indicates a date/time field, "FD" is a float (double) value, etc. The **Value Representation** for a field can be looked up either in a standard dictionary (provided by the DICOM.jl module), or it might be overwritten by a custom dictionary embedded in the DICOM file.

DiKoala looks up **Attributes** in a DICOM file. An **Attribute** is composed of the following constituents:
- An **Attribute Name**. This is a human-readable descriptive string describing the information contained in the **Attribute**, such as *Patient Name* or *Study Date*. The standard also mentions the term **Keyword** for the **AttributeName** with all whitespaces removed, e.g., *PatientName*. Since this form is used for looking up **Attributes** in DICOM.jl, we will use the term **Keyword** for a string that identifies an **Attribute**.
- A **Data Element Tag** (see above) that identifies the **Attribute** in a computer-readable format.

### Summary
To summarize, here is an example for an **Attribute*:

| **Attribute Name** | **Keyword** | **Data Element Tag** (Group Number, Element Number) | **Value Representation** | Contents of the **Value Field** (example) |
|---|---|---|---|---|
|Sequence Name | SequenceName | (0018,0024) | "SH" (short string) | *fl3d1_ns |
