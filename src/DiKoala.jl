

module DiKoala
using DiKoala
include("Types.jl")
include("DicomStuff.jl")
include("BinaryExpressionTree.jl")
include("ConditionParser.jl")
include("TreeVisualization.jl")

include("DirectoryScanner.jl")


function dikoala_main()

    cli_args = parse_cli_arguments()
    options = copy(cli_args)

    matches = perform_scan(options)

    if length(matches) > 0
        print_results(matches, options)
        if !options["no-info"]
            n = length(matches)
            match_word = n==1 ? "match" : "matches"
            println("Found $n $match_word.")
        end
    elseif !options["no-info"]
        print("No matches found.")
        if !options["extended"]
            print(". Maybe use the -x/--extended switch to scan nested header structures.")
        end
        println()
    end
end

##### The program #####
if abspath(PROGRAM_FILE) == @__FILE__
    # this is the julia equivalent to the __name__ == "__main__" guard in Python.

    # call main only if this file is executed. 
    
    dikoala_main()

end

end  # module DiKoalaMain