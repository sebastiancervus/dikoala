#=
BinaryExpressionTree:
- Julia version: 1.6.2
- Author: Sebastian Hirsch
- Date: 2021-09-14
=#


"""
To represent queries of the form "StudyDate >= 2021-03-14", we use a tree structure. The process of
"evaluating" a tree refers to applying a function evaluate_node to all nodes in a bottom-up fashion.
There are three types of nodes within a tree (all derived from abstract base class ExprTreeNode):
- ConstantNode - representing a single constant value, such as 2021-03-04, a string, or a number.
When a ConstantNode is being evaluated, it simply returns its stored value.
- HeaderNode - representing a DICOM Attribute (via its Keyword, cf. DICOM Nomenclature.md).
When being evaluated, a HeaderNode extracts the corresponding value stored under that Attribute in a
DICOM file.
- ConditionNode: A ConditionNode stores exactly 2 children and a binary operation. When being
evalueted, a ConditionNode evaluates both its children, and uses the return values as input to the
binary operation. The output of the operation is the value of the condition node.

Leaf nodes can be either ConstantNodes or HeaderNodes. All internal nodes have to be ConditionNodes,
since they are the only nodes that can have children.

Example: The above condition "StudyDate >= 2021-03-04" is represented as

                            ConditionNode with operation >=
                                |                   |
                                |                   |
                          HeaderNode             ConstantNode
                   with Keyword "StudyDate"   with value 2021-03-04

When the tree is evaluated for a given DICOM file, the HeaderNode returns the value of the StudyDate
Attribute in the DICOM header. The ConstantNode returns 2021-03-04. The ConditionNode then performs
the operation >= on these two values and returns the result. A helper function converts the value
of the ConstantNode to the same data type as the output as the HeaderNode (in this example, a Dates.Date
type), so that the two values can be compared. If the result of the binary operation is true, then the DICOM
files matches the query condition, otherwise it does not.

Compound queries, such as "(StudyDate>=2021-03-04) and (PatientName contains a)", are analogously
represented as

                                        ConditionNode with operation "and (&&)"
                                         |                                  |
                                         |                                  |
                      ConditionNode with operation >=               ConditionNode with operation "contains"
                         |                    |                           |                     |
                         |                    |                           |                     |
                HeaderNode with           ConstantNode with         HeaderNode with          ConstantNode
                keyword "StudyDate"       value 2021-03-04          keyword PatientName      with value "a"

A ConditionNode can either have two other ConditionNodes as children, or one HeaderNode (left child)
and one ConstantNode (right child).
"""

using DICOM

#include("./DicomStuff.jl")
#include("./Types.jl")
using Dates

@enum SIDE SIDE_LEFT SIDE_RIGHT

abstract type ExprTreeNode
end

struct ConditionNode <:ExprTreeNode
    left::ExprTreeNode
    op  # a callable with two arguments, but there does not seem to be a type for it?
    right::ExprTreeNode
    op_str::String  # a string representation of the operation

    #negate::Bool
end

ConditionNode(left::ExprTreeNode, op::Any, right::ExprTreeNode) = ConditionNode(left, op, right, string(op))

struct ConstantNode <:ExprTreeNode
    value::Any
end

struct HeaderNode <:ExprTreeNode
    keyword::String
end

mutable struct SymbolicNode <:ExprTreeNode
    contents_str::String
    operation::Any
    parent::Union{SymbolicNode,Nothing}
    side::SIDE
    left::Union{SymbolicNode,Nothing}  # nullable
    right::Union{SymbolicNode,Nothing}  # nullable
end

# convenice constructor without children
SymbolicNode(contents_str::String, operation::Any, parent::Union{SymbolicNode,Nothing}, side::SIDE) = SymbolicNode(contents_str, operation, parent, side, nothing, nothing)

function evaluate_node(n::ConditionNode, dcm_dict::DICOM.DICOMData, options::OptionType=OptionType())

    return_value = EvaluationItem()
    # val_l and val_r are HeaderLookupResults
    val_l = evaluate_node(n.left, dcm_dict, options)
    val_r = evaluate_node(n.right, dcm_dict, options)

    # we convert the rhs value (a constant) to the same type as the lhs value. If val_r is not a
    # String, we assume that it already has the right type and does not need a conversion.

    ret_val = false
    matched_values = MultipleLookupValues()
    if isa(n.left, HeaderNode) && isa(n.right, ConstantNode)
        
        vals = val_l.values  # this is a Dict.
        
        for val_vec in values(vals)  # x is a vector
            for x in val_vec
                v = val_r.node_info  # a ConstantNode only produces a single value. v might be overwritten below, so we need to re-initialize for each iteration of the for loop.

                if typeof(v) == String
                    if typeof(x) === Nothing  # this means that the DICOM Attribute could not be resolved for this file, so the test will be considered failed (return false)
                        continue
                    end
                                                
                    v = convert_type(typeof(x), v)
                end
                
                if n.op(x, v)
                    ret_val = true
                    add_to_dict!(matched_values, n.left.keyword, x)
                end
            end
        end
        return_value = EvaluationItem(match=ret_val, node_info=n.left.keyword, values=matched_values)
    elseif isa(n.left, ConditionNode) && isa(n.right,ConditionNode)
        # ConditionNodes return a single boolean value encapsulated in a Vector
        ret_val = n.op(val_l.match, val_r.match)
        combined_dict = val_l.values

        if ret_val
            merge_dict!(combined_dict, val_r.values)
        end

        return_value = EvaluationItem(match=ret_val, node_info="", values=combined_dict)
    end

    return_value
end

function evaluate_node(n::ConstantNode, dcm_dict::DICOM.DICOMData, options::OptionType=OptionType())

    val = n.value
    descr = "Const: $val"

    # Use a named tuple as a return structure
    result = EvaluationItem(match=true, node_info=val, values=MultipleLookupValues())
    result
    
 end

function evaluate_node(n::HeaderNode, dcm_dict::DICOM.DICOMData, options::OptionType=OptionType())

    results = HeaderLookupResults()
    if get(options, "extended", false)
        results = header_lookup_extended(dcm_dict, n.keyword)
    else
        results = header_lookup_standard(dcm_dict, n.keyword)
    end

    val = [parse_field(x[1], x[2]) for x in results]

    d = MultipleLookupValues(n.keyword => val)

    matched = !isempty(results) && !isempty(results[1])

    # return a tuple:
    result = EvaluationItem(match=matched, node_info=n.keyword, values=d)
end

"""
get_max_dicom_group(node::ExprTreeNode)::UInt16
Extract the maximum GroupNumber referenced by HeaderNodes in node and (potentially) its subnodes.
"""
function get_max_dicom_group(node::ExprTreeNode)::UInt16

    if isa(node, HeaderNode)
        group_hex, _ = DICOM.fieldname_dict[Symbol(node.keyword)]
        return group_hex
    elseif isa(node, ConditionNode)
        return max(get_max_dicom_group(node.left), get_max_dicom_group(node.right))
    end

    UInt16(0x0)  # for ConstantNodes
end


"""
make_date(s::String)::Date

Construct a date object from String s. The expected format is "yyyymmdd", i.e., 20210915, or
"yyyy-mm-dd"\n
Returns: a Date object
"""
function make_date(s::String)::Date

    if match(r"^\d{4}-\d{2}-\d{2}$", s) !== nothing
        return Date(s, "yyyy-mm-dd")
    elseif match(r"^\d{8}$", s) !== nothing
        return Date(s, "yyyymmdd")
    else
        throw(ErrorException("Invalid date string: \"$s\". Valid formats are yyyy-mm-dd or yyyymmdd."))
    end
end


"""
make_time(s::String)::Time

Construct a time object from String s. The expected format is either HHMMSS.s or HH:MM:SS.s,
where "s" is the (optional) fractional part of a second (arbitrary number of decimals, or none).\n
Returns: a Time object
"""
function make_time(s::String)::Time
    if occursin(r"\d{6}(\.(\d*))?", s)  # check for format HHMMSS.s (with optional fractional part
        return Time(s, "HHMMSS.s")
    else  # Alternative format is HH.MM.SS.s
        return Time(s, "HH:MM:SS.s")
    end
end  # make_time


"""
make_datetime(date_str::String, time_str::String)::DateTime

Construct a DateTime object from two strings (date_str, time_str). The first one encodes the
date in yyyymmdd format, and the second one in either HHMMSS.s or HH:MM:SS.s format. The fractional
part (.s) is optional in both cases.

Returns: a DateTime object
"""
function make_datetime(date_str::String, time_str::String)::DateTime

    date_part = make_date(date_str)
    time_part = make_time(time_str)
    return DateTime(date_part, time_part)

end  # make_datetime

"""
make_datetime(date_time_str::String)::DateTime

Construct a DateTime object from a single string "Date Time", where\n
Date = yyyy-mm-dd or yyyymmdd\n
Time = HHMMSS.s  (fractional part .s is optional)\n
Date and time must be separated by one or more whitespaces.

Returns: a DateTime object
"""
function make_datetime(date_time_str::String)::DateTime

    date_time_str = strip(date_time_str)  # remove leading and trailing whitespace
    parts = split(date_time_str, r"\s+")  # split into date and time part
    make_datetime(String(parts[1]), String(parts[2]))

end
"""
make_condition(keyword::String, op::Symbol, val::Any)::ConditionNode

Construct a ConditionNode using the structure `keyword op val`, where keyword is a DICOM Attribute
(referenced via its Keyword), op is an arbitrary binary operator that returns a Bool, and val is
some value of the same type as will be returned by evaluate_node(::HeaderNode...) would return for
the given Attribute (i.e., for keyword="StudyDate" it would be a Dates.Date object, for "StudyTime"
a Dates.Time object, etc.). The val will be wrapped in a ConstantNode.

Returns: a ConditionNode representing the desired condition
"""
function make_condition(keyword::String, op::Symbol, val::Any)::ConditionNode
    hn = HeaderNode(keyword)
    vn = ConstantNode(val)

    ConditionNode(hn, op, vn, string(op))

end


"""
convert_type(T::Type, val::String)::T

Try to convert String val to type T. Depending on T, different conversion strategies will be used.

"""
function convert_type(T::Type, val::String)::T

    if T == String || T == Nothing
        return val
    elseif T <: Real  # numerical type
        v = parse(T, val)
        return v
    elseif T == Dates.Date
        return make_date(val)
    elseif T == Dates.Time
        return make_time(val)
    elseif T == Dates.DateTime
        return make_datetime(val)
    else
        throw(ErrorException("Do not have a conversion from String to $T ($val), val[1]=$(val[1]), val[2] = $(val[2])"))
    end

end

# Some helper functions
# test if f is a logical and function
is_and(f)::Bool = try
        f(true, true) && !f(true,false) && !f(false,true) && !f(false,false)
    catch err
        false
    end

# test if f is a logical or function
is_or(f)::Bool = try
        f(true,true) && f(true,false) && f(false,true) && !f(false,false)
    catch err
        false
    end

# test if f is a "contains function" (verrry cheapo version!)
is_contains(f)::Bool = try
        f("Hello", "e") && !f("Hello", "x")
    catch err
        false
    end
