#=
DiKoala:
- Julia version: 1.6.2
- Author: Sebastian Hirsch
- Date: 2021-09-16
- This file is licensed under the 3-clause BSD license. See License.md for the full license text.
=#

#=
module DiKoala
include("DicomStuff.jl")
include("BinaryExpressionTree.jl")
include("ConditionParser.jl")
include("TreeVisualization.jl")
include("Types.jl")
=#

using Dates
using ArgParse
using DICOM

@enum RUN_MODE FILTER COLLECT

"""
scan_directory(start_dir::String, fun; filemode::Bool=false, until_dicom_group::UInt16=UInt16(0x0))
Perform a recursive scan starting at start_dir, and apply unary function fun to each DICOM file
(if filemode==true) or the first DICOM file in every directory (filemode==false).

The results for each function evaluation are returned as a Vector of EvaluationItem's. If the optional
parameter until_dicom_group is specified, the DICOM header is only read up to that Group Number,
rather than reading the entire header. This can provide a speed-up, but it does not work with the
extended search mode for nested DICOM header structures (see -x/--extended command line option).
"""
function scan_directory(start_dir::String, fun; filemode::Bool=false, until_dicom_group::UInt16=UInt16(0x0))::Vector{EvaluationItem}

    results = Vector{EvaluationItem}()

    for (root, dirs, files) in walkdir(start_dir, topdown=false, follow_symlinks=true)

        for f in files
            break_after_file = false  # used for directory mode to break from the loop after the first DICOM file has been inspected
            full_path = joinpath(root, f)

            # DICOM.isdicom crashes if the file size is less than 3 bytes, so we check this explicitly.
            try
                if !DICOM.isdicom(full_path)
                    continue
                end
            catch err
                if isa(err, BoundsError)  # this happens if the size of full_file is < 3 bytes
                    continue
                else
                    println("An error occurred during DICOM.isdicom($full_path). Error message: $err")
                end
            end

            result_for = filemode ? full_path : root  # report the result either for the file (in file mode) or for the containing directory

            dcm_data = nothing

            dcm_data = read_dicom_header(full_path, until_dicom_group)

            if !filemode
                break_after_file = true
            end

            result = fun(dcm_data)
            result.target = result_for

            if result.match
                push!(results, result)

            end

            if break_after_file
                break
            end
        end
    end  # for ... walkdir

    return results
end

"""
parse_cli_arguments()::OptionType
Parse the provided command line options and return the result as a Dict{String, Any}.
"""
function parse_cli_arguments()::OptionType
    s = ArgParseSettings()
    @add_arg_table! s begin

    "--dirmode", "-d"
        help = "Use dir mode, i.e., only inspect the first file of every directory. Default: off (i.e., inspect every file)"
        action = :store_true

    "--no-info", "-n"
        help = """Only print the files matching the condition, and no other information
        Hint: Use this option to produce list of files that can be piped into other commands, such as xargs.
        """
        action = :store_true

    "--reverse", "-r"
        help = "Reverse order of results. May be particularly useful in conjunction with --collect"
        action = :store_true

    "--path", "-p"
        help = "The path which will be recursively scanned for DICOM files. If omitted, the current working directory will be used."
        default = pwd()

    "--show-condition-tree", "-t"
        help = "Print the tree representation of the condition string and exit"
        action = :store_true

    "--extended", "-x"
        help = "Extended search. Look also in nested DICOM structures, such as used by Siemens scanners using the XA software line (e.g., Lumina, Vida). Note that this option will slow down the search significantly."
        action = :store_true

    "condition_string"
        help = "A condition as a string, enclosed by quotation marks"

    end
    pa = parse_args(s)

    if pa["condition_string"] === nothing
        println("Error: No condition has been specified. Please use --help to get a summary of the syntax.")
        exit(0)
    end
    pa

end

"""
print_results(res::Vector, options::Dict)::Nothing
Print the results in the vector res (should be a Vector{ResultItem} type). The options Dict can contain
settings that effect the outcome or formatting of the printing.
"""
function print_results(res::Vector, options::Dict)::Nothing

    function format_tuple(t)
        s = ""
        d = t.values

        lines = Vector{Tuple{String, String}}()

        for (k, v) in d
            push!(lines, (k, join([string(x) for x in v], ", ")))
        end

        sort!(lines, by=x->x[1])
        
        lines = map(x -> "$(x[1]) -> $(x[2])", lines)
        
        return_string = join(lines, "; ")

    end

    print_attribute_values = !get(options, "no-info", false)
    
    for x in res
         property = format_tuple(x)
        
        if options["mode"] == COLLECT
            println(property)
        elseif print_attribute_values
            println("$(x.target): $property")
        else
            println(x.target)
        end
    end
end # print_results

"""
collect_results(res::Vector; rev=false)
In collect mode, this function processes the results of the scan_directory function and generates
statistics on how often each result occurred. The results are returned, ordered by number of occurrences,
in ascending (rev==false) or descending (rev==true) and can then be printed using print_results.
"""
function collect_results(res::Vector; rev=false)::Vector{EvaluationItem}

    counts = Dict{Any, Int64}()

    # first collect values in a dict for easy counting
    # TODO: Explain loop structure!
    for r in res  # r is an EvaluationItem
        for (k, v) in r.values
            for vv in v
                if haskey(counts, vv)
                    counts[vv] += 1
                else
                    counts[vv] = 1
                end
            end
        end
    end

    results_list = [EvaluationItem(target=string(x[2]), match=true, node_info=string(x[1]), values=MultipleLookupValues(string(x[1]) => [x[2]])) for x in counts]  # make a list of tuples (value, count). Note that x is Pair{String, Int64}, we need to convert to Tuple{String, Int64})
    # the explicit string conversions in the line above are necessary, since x[1] can be a non-String type, for example a DateObject is StudyDate is queried.
    sort!(results_list, by=x->x.target, rev=rev)
end

"""
print_tree(node::ExprTreeNode)
Print a text representation of the internally generated condition stree, starting at node as the root.
"""
function print_tree(node::ExprTreeNode)::Nothing
    print_node(node)
end

"""
Prepare scanning a directory. Based on the provided options, different parameters are set, the
condition tree is generated and the scan function is called with the appropriate settings.
"""
function perform_scan(options::OptionType)::Vector{EvaluationItem}

    start_folder = options["path"]

    node = make_condition_tree(options["condition_string"])

    if get(options, "show-condition-tree", false)
        print_tree(node)
        exit(0)
    end

    if !get(options, "no-info", false)
        println("Scanning directory $start_folder")
    end
    
    max_header_group = get(options, "extended", false) ? typemax(UInt16) : get_max_dicom_group(node)

    options["mode"] = isa(node, ConditionNode) ? FILTER : COLLECT

    fun(x) = evaluate_node(node, x, options)
    
    matches = scan_directory(start_folder, fun, filemode=!get(options, "dirmode", false), until_dicom_group=max_header_group)

    if options["mode"] == COLLECT
        matches = collect_results(matches, rev=get(options, "reverse", false))
    end

    matches
end


"""
The main method, serving as the entry point when this file is called as a program.
"""

#=
function dikoala_main()

    cli_args = parse_cli_arguments()
    options = copy(cli_args)

    matches = perform_scan(options)

    if length(matches) > 0
        print_results(matches, options)
        if !options["no-info"]
            n = length(matches)
            match_word = n==1 ? "match" : "matches"
            println("Found $n $match_word.")
        end
    elseif !options["no-info"]
        print("No matches found.")
        if !options["extended"]
            print(". Maybe use the -x/--extended switch to scan nested header structures.")
        end
        println()
    end
end

=#

#=
##### The program #####
if abspath(PROGRAM_FILE) == @__FILE__
    # this is the julia equivalent to the __name__ == "__main__" guard in Python.

    # call main only if this file is executed. 
    
    dikoala_main()

end
=#
#end  # module DiKoala