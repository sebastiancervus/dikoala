#=
test_starter.jl:
- Julia version: 1.6.2
- Author: Sebastian Hirsch
- Date: 2021-09-16
- This file is licensed under the 3-clause BSD license. See License.md for the full license text.
=#

"""
This file simulates the test that is run by the CI pipeline of gitlab (but it will not be used
there, it only serves for local validation of the testing pipeline).
"""

using Pkg;
Pkg.activate(".")
Pkg.add("TestReports")

using TestReports
TestReports.test("DiKoala", coverage=false)